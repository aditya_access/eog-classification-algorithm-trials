import dynamic_time_warping


def half_cycles_duration(wave):
    high_count = low_count = zero_count = 0
    for i in range(len(wave)):
        if wave[i] < 0:
            low_count += 1
        if wave[i] == 0:
            zero_count += 1
        if wave[i] > 0:
            high_count += 1
    return high_count, low_count


def biggest_smallest_values(wave):
    return max(wave), min(wave)


def total_duration(wave):
    return len(wave)


def get_similarity_cost(wave, medoid):
    dtw_unnorm, dtw_norm, maxVal, maxValue = dynamic_time_warping.DDTW_SakoeChiba(wave, medoid)
    return dtw_unnorm, dtw_norm, maxVal, maxValue


def get_features(wave, medoid):
    positive_dur, negative_dur = half_cycles_duration(wave)
    big, small = biggest_smallest_values(wave)
    wave_duration = total_duration(wave)
    cost_un, cost_n, maxVal, maxValue = get_similarity_cost(wave, medoid)
    return [positive_dur, negative_dur, big, small, wave_duration, cost_un]
