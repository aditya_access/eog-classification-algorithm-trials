import config
from sklearn.neighbors import NearestCentroid
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from keras.models import Sequential
from keras.layers import Dense


def get_model_SVM(X, y):
    model_SVM = svm.SVC(kernel='linear', C=1.0, degree=5).fit(X, y)
    return model_SVM


def get_model_ANN(X, y, n_epochs):
    model_ANN = Sequential()
    model_ANN.add(Dense(12, input_dim=6, activation='sigmoid'))
    model_ANN.add(Dense(8, activation='sigmoid'))
    model_ANN.add(Dense(8, activation='sigmoid'))
    model_ANN.add(Dense(1, activation='sigmoid'))
    model_ANN.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model_ANN.fit(X, y, epochs=n_epochs, batch_size=1, verbose=0)
    return model_ANN


def get_model_kNN(X, y):
    model_kNN = KNeighborsClassifier(n_neighbors=3).fit(X, y)
    return model_kNN


def get_model_kNCN(X, y):
    model_kNCN = NearestCentroid(metric='euclidean', shrink_threshold=None).fit(X, y)
    return model_kNCN


def get_model_LR(X, y):
    model_LR = LinearRegression().fit(X, y)
    return model_LR


def get_model_Bayes(X, y):
    model_Bayes = GaussianNB().fit(X, y)
    return model_Bayes


def get_model_SGD(X, y):
    model_SGD = SGDClassifier().fit(X, y)
    return model_SGD


def get_model_DTree(X, y):
    model_DTree = DecisionTreeClassifier().fit(X, y)
    return model_DTree


def get_model_RForest(X, y):
    model_RForest = RandomForestClassifier().fit(X, y)
    return model_RForest


# for i in range(len(config.X_test)):
#     pred = config.model_SVM.predict(config.X_test[i])
#     print('Expected: {0}\tPredicted: {1}'.format(config.y_test[i], pred))

