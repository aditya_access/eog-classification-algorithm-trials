import matplotlib.pyplot as plt
import random


def model_1(convergence, seed, step_size):
    x = seed
    xs = [x]
    while x != convergence:
        if x < convergence:
            x -= step_size
        else:
            x += step_size
        xs.append(x)
    return xs


period = model_1(55, 0, 1)
# print(period)
plt.plot(period)
plt.show()
