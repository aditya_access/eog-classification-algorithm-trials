import config
import data_preprocessing
import csv


def get_cleaned_data(wave):
    paths = {
        'pb': "/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/cleanedup_pb.csv",
        'up': "/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/cleanedup_up.csv"
    }
    df = []
    with open(paths[wave], 'r') as f:
        csv_reader = csv.reader(f, delimiter=',')
        for lines in csv_reader:
            a = []
            for val in lines:
                if val == '':
                    a.append(None)
                else:
                    a.append(float(val))
            df.append(a)
    df = data_preprocessing.separate_by_column(df)
    return df