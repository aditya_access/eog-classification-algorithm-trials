import config
import csv

dataset_paths = {
    'x_train_dba': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/x_train_dba.csv',
    'x_test_dba': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/x_test_dba.csv',
    'y_train_dba': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/y_train_dba.csv',
    'y_test_dba': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/y_test_dba.csv',
    'x_train_hm': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/x_train_hm.csv',
    'x_test_hm': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/x_test_hm.csv',
    'y_train_hm': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/y_train_hm.csv',
    'y_test_hm': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/y_test_hm.csv',
}

dtw_max_paths = {
    'pb': {'lengths':'/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxLen_pb.csv',
           'values': '/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxVal_pb.csv'},
    'up': {'lengths' :'/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxLen_up.csv',
           'values' :'/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxVal_up.csv'}
}


def get_dataset(train_path, test_path):
    global dataset_paths
    train = []
    label_tr = []
    label_ts = []
    test = []
    with open(dataset_paths[train_path], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            a = []
            for t in s:
                a.append(float(t))
            train.append(a)
    with open(dataset_paths[test_path], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            a = []
            for t in s:
                a.append(float(t))
            test.append(a)
    with open(dataset_paths['y_train_dba'], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            label_tr.append(int(s[0]))
    with open(dataset_paths['y_test_dba'], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            label_ts.append(int(s[0]))
    return train, test, label_tr, label_ts


def dtw_maxes():
    global dtw_max_paths
    X_train = {'lengths': [], 'values': []}
    X_test = {'lengths': [], 'values': []}
    lengths = []
    values = []

    with open(dtw_max_paths['pb']['lengths'], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            lengths.append(int(s[0]))
    with open(dtw_max_paths['up']['lengths'], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            lengths.append(int(s[0]))
    with open(dtw_max_paths['pb']['values'], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            values.append(float(s[0]))
    with open(dtw_max_paths['up']['values'], "r") as f:
        rows = csv.reader(f)
        for s in rows:
            values.append(float(s[0]))

    for i in range(len(lengths)):
        if i <= 3 or i >= len(lengths) - 4:
            X_train['lengths'].append(lengths[i])
            X_train['values'].append(values[i])
        else:
            X_test['lengths'].append(lengths[i])
            X_test['values'].append(values[i])

    return X_train, X_test


# xtr, xts, ytr, yts = get_dataset('x_train_dba', 'x_test_dba')
# print(*yts, sep='\n')
# xtr, xts = dtw_maxes()
# for i, j in xts.items():
#     print(j)
