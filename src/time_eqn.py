import matplotlib.pyplot as plt
import math
import random
from scipy.integrate import odeint
import numpy as np

random.seed(1)


def iterative_eqn(x, y, k):
    z = math.sin(k * x) * y
    period = [z]
    for i in range(1, 1000):
        c = random.choice([True, False])
        if c:
            r = random.uniform(-0.05, 0.05)
            period.append(math.sin(k * r) * y)
        period.append(math.sin(k * period[i - 1]) * y)
    return period


def model_diverge(x, t):
    dydt = math.exp(x)
    return dydt


def model_converge(x):
    f = math.exp(-x)
    period = [f]
    for i in range(1, 100):
        period.append(math.exp(-period[i-1]))
    return period

# y0 = 0.05
# t = np.linspace(0, 300)
# m1 = odeint(model, y0, t)
m2 = model_converge(0.05)
# print(m1, sep='\n')
plt.plot(m2)
plt.show()