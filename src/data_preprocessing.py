import math
import config
# import dynamic_time_warping


def get_mean(series):
    _sum = 0
    for i in series:
        _sum += i
    return _sum / len(series)


def get_std(series, mean):
    _sum = 0
    for i in series:
        _sum += (i - mean) ** 2
    _sum = _sum / len(series)
    return math.sqrt(_sum)


def separate_by_column(series):
    lst2 = []
    for i in range(len(series[0])):
        a = []
        for j in range(len(series)):
            if series[j][i] is None:
                break
            a.append(series[j][i])
        lst2.append(a)
    return lst2


def get_Zscore(feature_set):
    mean = []
    std_dev = []
    col = separate_by_column(feature_set)
    standardised_set = []
    for i in range(len(col)):
        series = [j for j in col[i]]
        mean.append(get_mean(series))
        std_dev.append(get_std(series, mean[i]))
        standardised_set.append([((elem - mean[i]) / std_dev[i]) for elem in series])
    return separate_by_column(standardised_set)


def normalise(feature_set, maxVal, maxLen, denormalise=False):
    col = separate_by_column(feature_set)
    normalised_set = []
    for i in range(len(col[:-1])):
        if denormalise:
            normalised_set.append([(elem * (max(col[i]) - min(col[i])) + min(col[i])) for elem in col[i]])
        else:
            normalised_set.append([((elem - min(col[i])) / (max(col[i]) - min(col[i]))) for elem in col[i]])
    normalised_set.append([dynamic_time_warping.normalize_cost(elem, val, length) for elem, val, length in zip(col[5], maxVal, maxLen)])
    return separate_by_column(normalised_set)