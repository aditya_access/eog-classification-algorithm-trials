import dynamic_time_warping
import config
import data_preprocessing
import signal_input
import extract_features
import medoid_calc
import csv
import pandas as pd


def run():
    all_waves_pb = signal_input.get_cleaned_data('pb')
    all_waves_up = signal_input.get_cleaned_data('up')
    hmedoid_10, hmedoid_5 = medoid_calc.get_hmedoid()
    dbamedoid_10, dbamedoid_5 = medoid_calc.get_DBA()
    features_pb_dba = []
    features_up_dba = []
    features_pb_hm = []
    features_up_hm = []
    maxVal_pb = []
    maxVal_up = []
    maxLen_pb = []
    maxLen_up = []

    for i in range(len(all_waves_up)):
        features_up_dba.append(extract_features.get_features(all_waves_up[i], dbamedoid_10))
        features_up_hm.append(extract_features.get_features(all_waves_up[i], hmedoid_10))
        # maxVal_up.append(features_up_hm[i][5][1])
        # maxLen_up.append(features_up_hm[i][5][2])
    for idx, i in enumerate(all_waves_pb):
        features_pb_dba.append(extract_features.get_features(i, dbamedoid_10))
        features_pb_hm.append(extract_features.get_features(i, hmedoid_10))
        # maxVal_pb.append(features_pb_hm[idx][5][1])
        # maxLen_pb.append(features_pb_hm[idx][5][2])

    # with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxLen_pb.csv",
    #           "w") as f:
    #     writer = csv.writer(f)
    #     writer.writerows(map(lambda x: [x], maxLen_pb))
    # with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxLen_up.csv",
    #           "w") as f:
    #     writer = csv.writer(f)
    #     writer.writerows(map(lambda x: [x], maxLen_up))
    # with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxVal_pb.csv",
    #           "w") as f:
    #     writer = csv.writer(f)
    #     writer.writerows(map(lambda x: [x], maxVal_pb))
    # with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxVal_up.csv",
    #           "w") as f:
    #     writer = csv.writer(f)
    #     writer.writerows(map(lambda x: [x], maxVal_up))

    # print(*maxLen_pb, sep='\n')
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/features/features_pb_dba.csv",
              "w") as f:
        writer = csv.writer(f)
        writer.writerows(features_pb_dba)
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/features/features_pb_hm.csv",
              "w") as f:
        writer = csv.writer(f)
        writer.writerows(features_pb_hm)
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/features/features_up_dba.csv",
              "w") as f:
        writer = csv.writer(f)
        writer.writerows(features_up_dba)
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/features/features_up_hm.csv",
              "w") as f:
        writer = csv.writer(f)
        writer.writerows(features_up_hm)


def run_2():
    res = []
    maxVals = []
    maxLens = []
    with open('/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/features/features_pb_dba.csv') as csvfile:
        rows = csv.reader(csvfile)
        for s in rows:
            a = []
            for t in s:
                a.append(float(t))
            res.append(a)
    with open('/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxLen_pb.csv') as f:
        rows = csv.reader(f)
        for s in rows:
            a = []
            for t in s:
                a.append(float(t))
            maxLens.append(a[0])
    with open('/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/DTW_vals/maxVal_pb.csv') as f:
        rows = csv.reader(f)
        for s in rows:
            a = []
            for t in s:
                a.append(float(t))
            maxVals.append(a[0])
        # print(*res, sep='\n')
    res2 = data_preprocessing.get_Zscore(res)
    for i in res2:
        for j in i:
            print(round(j, 5), end=',\t')
        print()
    # print(*res2, sep='\n')


def run_3():
    X_train = []
    y_train = []
    X_test = []
    y_test = []
    p1 = r"/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/features/features_pb_hm.csv"
    p2 = r"/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/features/features_up_hm.csv"
    X = []
    y = []
    with open(p1, "r") as f:
        csv1 = csv.reader(f)
        for s in csv1:
            tmp1 = []
            for t in s:
                tmp1.append(float(t))
            X.append(tmp1)
            y.append(1)
    with open(p2, "r") as f:
        csv2 = csv.reader(f)
        for s in csv2:
            tmp2 = []
            for t in s:
                tmp2.append(float(t))
            X.append(tmp2)
            y.append(0)
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/X_hm.csv", "w") as f:
        w = csv.writer(f)
        w.writerows(X)
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/y_hm.csv", "w") as f:
        w = csv.writer(f)
        w.writerows(map(lambda y: [y], y))

    for i in range(len(X)):
        if i <= 3 or i >= len(X) - 4:
            X_train.append(X[i])
            y_train.append(y[i])
        else:
            X_test.append(X[i])
            y_test.append(y[i])

    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/x_train_hm.csv", "w") as f:
        w = csv.writer(f)
        w.writerows(X_train)
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/x_test_hm.csv", "w") as f:
        w = csv.writer(f)
        w.writerows(X_test)
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/y_train_hm.csv", "w") as f:
        w = csv.writer(f)
        w.writerows(map(lambda y: [y], y_train))
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/main_datasets/y_test_hm.csv", "w") as f:
        w = csv.writer(f)
        w.writerows(map(lambda y: [y], y_test))


# run_3()
