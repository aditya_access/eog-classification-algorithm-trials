import matplotlib.pyplot as plt
from matplotlib import style
import seaborn as sns
import csv
import numpy as np
import data_preprocessing
style.use('seaborn')


def get_average_acc():
    one = []
    five = []
    ten = []
    twenty = []
    hundred = []
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/ANN/hm/3hl/1epochs/100trials.csv", "r") as f:
        r = csv.reader(f)
        for s in r:
            one.append(float(s[0]))
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/ANN/hm/3hl/5epochs/100trials.csv", "r") as f:
        r = csv.reader(f)
        for s in r:
            five.append(float(s[0]))
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/ANN/hm/3hl/10epochs/100trials.csv", "r") as f:
        r = csv.reader(f)
        for s in r:
            ten.append(float(s[0]))
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/ANN/hm/3hl/20epochs/100trials.csv", "r") as f:
        r = csv.reader(f)
        for s in r:
            twenty.append(float(s[0]))
    with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/ANN/hm/3hl/100epochs/100trials.csv", "r") as f:
        r = csv.reader(f)
        for s in r:
            hundred.append(float(s[0]))

    epochs = [round((sum(one)/len(one)) * 100, 5),
              round((sum(five)/len(five)) * 100, 5),
              round((sum(ten)/len(ten)) * 100, 5),
              round((sum(twenty)/len(twenty)) * 100, 5),
              round((sum(hundred)/len(hundred)) * 100, 5)]
    means = [data_preprocessing.get_mean(one) * 100,
             data_preprocessing.get_mean(five) * 100,
             data_preprocessing.get_mean(ten) * 100,
             data_preprocessing.get_mean(twenty) * 100,
             data_preprocessing.get_mean(hundred) * 100]
    std_dev = [data_preprocessing.get_std(one, data_preprocessing.get_mean(one)) * 100,
               data_preprocessing.get_std(one, data_preprocessing.get_mean(five)) * 100,
               data_preprocessing.get_std(one, data_preprocessing.get_mean(ten)) * 100,
               data_preprocessing.get_std(one, data_preprocessing.get_mean(twenty)) * 100,
               data_preprocessing.get_std(one, data_preprocessing.get_mean(hundred)) * 100]
    labels = ['1', '5', '10', '20', '100']

    x = np.arange(len(labels))

    fig, ax = plt.subplots()
    acc = ax.bar(labels, epochs, width=0.4)
    ax.set_ylabel('Percentage Accuracy')
    ax.set_xlabel('Number of Epochs')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)

    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    autolabel(acc)
    fig.tight_layout()

    plt.show()
    # print(*x, sep='\n')

    for i, j in zip(means, std_dev):
        print('({0}, {1})'.format(round(i, 2), round(j, 2)))
        

get_average_acc()
