import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
import config
import data_preprocessing
import dynamic_time_warping
import medoid_calc
import signal_input
import prepare_dataset
import Learning_models
from sklearn.metrics import accuracy_score
import csv
import seaborn as sns
style.use('seaborn')

# LOAD WAVES
config.all_waves_pb = signal_input.get_cleaned_data('pb')
config.all_waves_up = signal_input.get_cleaned_data('up')

# LOAD MEDOIDS
config.hmedoid_10, config.hmedoid_5 = medoid_calc.get_hmedoid()
config.dbamedoid_10, config.dbamedoid_5 = medoid_calc.get_DBA()

# LOAD FEATURES
config.X_train, config.X_test, config.y_train, config.y_test = prepare_dataset.get_dataset('x_train_hm', 'x_test_hm')
dtw_maxtrain, dtw_maxtest = prepare_dataset.dtw_maxes()

# # NORMALISE DATASET
# config.X_train = data_preprocessing.normalise(config.X_train, dtw_maxtrain['values'], dtw_maxtrain['lengths'])
# config.X_test = data_preprocessing.normalise(config.X_test, dtw_maxtest['values'], dtw_maxtest['lengths'])

# STANDARDISE DATASET
config.X_train = data_preprocessing.get_Zscore(config.X_train)
config.X_test = data_preprocessing.get_Zscore(config.X_test)


# RESET MODELS
config.reset_models()
config.reset_accuracy()
config.reset_predictions()


# CONVERT DATASET LISTS TO NUMPY ARRAYS (ONLY NEEDED FOR THE ANN)
config.X_train = np.array(config.X_train)
config.y_train = np.array(config.y_train)
config.X_test = np.array(config.X_test)
# config.y_test = np.array(config.y_test)
acc_adder = 0
acc_array = []
n_epochs = 100
n_trials = 100
# PREDICTIONS
for i in range(n_trials):
    config.model_ANN = Learning_models.get_model_ANN(config.X_train, config.y_train, n_epochs)
    config.pred_ANN = config.model_ANN.predict_classes(config.X_test)
    config.acc_ANN = accuracy_score(config.y_test, config.pred_ANN)
    acc_adder += config.acc_ANN
    acc_array.append([config.acc_ANN])
print('average accuracy of ANN = {}'.format(acc_adder / n_trials))
acc_std = data_preprocessing.get_std(acc_array, (acc_adder / n_trials))
print('standard deviation of ANN accuracy = {}'.format(acc_std))
# with open("/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/ANN/hm/3hl/100epochs/100trials.csv", "w") as f:
#     rows = csv.writer(f)
#     rows.writerows(acc_array)

# print(config.X_train, sep='\n')
# for i, j in zip(config.X_test, config.y_test):
#     print('{0}\t{1}'.format(i, j))

