import csv
import glob
import pandas as pd
import config


def get_DBA():
    dbamedoid_10 = []
    dbamedoid_5 = []
    readCSV_10 = pd.read_csv(r"/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/medoids/DBA_medoid_10.csv")
    readCSV_05 = pd.read_csv(r"/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/medoids/DBA_medoid_5.csv")
    for i, j in zip(readCSV_10['DBA_medoid'].tolist(), readCSV_05['DBA_medoid'].tolist()):
        dbamedoid_10.append(i)
        dbamedoid_5.append(j)
    return dbamedoid_10, dbamedoid_5


def get_hmedoid():
    hmedoid_10 = []
    hmedoid_5 = []
    readCSV_10 = pd.read_csv(
        r"/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/medoids/hmedoid_medoid_10.csv")
    readCSV_05 = pd.read_csv(
        r"/home/adityak/Google_files_rc/Drowsy_Driving_Project/TEST_03_NEW/Programs/Python/paper_algorithm_v0.2/medoids/hmedoid_medoid_5.csv")
    for i, j in zip(readCSV_10['hmedoid_medoid'].tolist(), readCSV_05['hmedoid_medoid'].tolist()):
        hmedoid_10.append(i)
        hmedoid_5.append(j)
    return hmedoid_10, hmedoid_5
