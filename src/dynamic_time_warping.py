import config
import numpy as np
import csv
import time
import signal_input
import medoid_calc


def normalize_cost(cost, maxVal, maxLen):
    maxAll = maxLen * maxVal
    var = None
    try:
        var = (maxAll - cost) / maxAll
    except RuntimeWarning:
        print('Most likely the same wave')
    return var


def DDTW_SakoeChiba(X, Y):
    m = len(X)
    n = len(Y)
    w = max(5, abs(m - n))
    path = []
    path_elements = []
    r = 0
    s = 0
    metric = 0
    ddtw = np.matrix(np.ones((n,m)) * np.inf)
    ddtw[0, 0] = 0
    for i in range(1, n):
        for j in range(max(1, i - w), min(m, i + w)):
            ddtw[i, j] = 0
    for i in range(1, n):
        for j in range(max(1, i - w), min(m, i + w)):
            # if (i == 0 and j == 0) or (i == 0 and j != 0) or (j == 0 and i != 0):
            #     continue
            if i + 1 >= n or j + 1 >= m:
                continue
            # elif ddtw[i, j + 1] == np.inf or ddtw[i, j - 1] == np.inf or ddtw[i + 1, j] == np.inf or ddtw[i - 1, j] == np.inf:
            #     continue
            else:
                if ddtw[i, j] == np.inf:
                    continue
                elif ddtw[i, j + 1] == np.inf or ddtw[i + 1, j] == np.inf:
                    derDist = float((X[j] - Y[i]) ** 2)
                else:
                    dX = ((X[j] - X[j - 1]) + ((X[j + 1] - X[j - 1]) / 2.0)) / 2.0
                    dY = ((Y[i] - Y[i - 1]) + ((Y[i + 1] - Y[i - 1]) / 2.0)) / 2.0
                    derDist = float((dX - dY) ** 2)
                ddtw[i, j] = derDist + min(min(ddtw[i - 1, j], ddtw[i - 1, j - 1]), ddtw[i, j - 1])
                if ddtw[i - 1, j] == min(min(ddtw[i - 1, j], ddtw[i - 1, j - 1]), ddtw[i, j - 1]):
                    r = i - 1
                elif ddtw[i, j - 1] == min(min(ddtw[i - 1, j], ddtw[i - 1, j - 1]), ddtw[i, j - 1]):
                    s = j - 1
                else:
                    r = i - 1
                    s = j - 1
        # path.append([s, r])
        # path_elements.append(ddtw[r, s])
        # metric += ddtw[r, s]
        if ddtw[r, s] == np.inf:
            # print(i, '\t', metric)
            pass
        else:
            path.append([s, r])
            path_elements.append(ddtw[r, s])
            metric += ddtw[r, s]
    warpingCost = (metric ** 0.5)
    warpingCost_norm = normalize_cost(warpingCost, max(max(X), max(Y)), max(m, n))
    return warpingCost, warpingCost_norm, max(max(X), max(Y)), max(m, n)


def run():
    mat = signal_input.get_cleaned_data('pb')
    med10, med5 = medoid_calc.get_hmedoid()
    # print(*mat, sep='\n')
    t1 = time.process_time()
    for i in range(len(mat)):
        cost, n_cost, maxVal, maxLen = DDTW_SakoeChiba(med10, mat[i])
    t2 = time.process_time()
    print('average time required to computing ddtw for {0}: {1}'.format(len(mat), (t2-t1)))
    print('total time required to one sequence: {}'.format((t2-t1)/len(mat)))


run()
