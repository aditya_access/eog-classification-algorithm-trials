

# WAVE OBJECTS
single_wave = []
medoid_wave = []
wave_buffer = []
all_waves_pb = []
all_waves_up = []

# FEATURES
features_pb_dba = []
features_up_dba = []
features_pb_hm = []
features_up_hm = []


# HARMONIC MEDOID
hmedoid_5 = []
hmedoid_10 = []


# DBA MEDOID
dbamedoid_5 = []
dbamedoid_10 = []


# LEARNING MODELS
model_kNN = None
model_kNCN = None
model_SVM = None
model_ANN = None
model_LR = None
model_SGD = None
model_Bayes = None
model_DTree = None
model_RForest = None


def reset_models():
    global model_SGD, model_Bayes, model_LR, model_kNN, model_ANN, model_kNCN, model_SVM, model_RForest, model_DTree
    model_kNN = None
    model_kNCN = None
    model_SVM = None
    model_ANN = None
    model_LR = None
    model_SGD = None
    model_Bayes = None
    model_DTree = None
    model_RForest = None


# ACCURACY MEASURES
acc_kNN = 0
acc_kNCN = 0
acc_SVM = 0
acc_ANN = 0
acc_LR = 0
acc_SGD = 0
acc_Bayes = 0
acc_DTree = 0
acc_RForest = 0


def reset_accuracy():
    global acc_kNN, acc_kNCN, acc_SVM, acc_ANN, acc_LR, acc_SGD, acc_Bayes, acc_DTree, acc_RForest
    acc_kNN = 0
    acc_kNCN = 0
    acc_SVM = 0
    acc_ANN = 0
    acc_LR = 0
    acc_SGD = 0
    acc_Bayes = 0
    acc_DTree = 0
    acc_RForest = 0


# PREDICTION OBJECTS
pred_kNN = []
pred_kNCN = []
pred_SVM = []
pred_ANN = []
pred_LR = []
pred_SGD = []
pred_Bayes = []
pred_DTree = []
pred_RForest = []


def reset_predictions():
    global pred_ANN, pred_RForest, pred_DTree, pred_LR, pred_SGD, pred_Bayes, pred_kNCN, pred_kNN, pred_SVM
    pred_kNN = []
    pred_kNCN = []
    pred_SVM = []
    pred_ANN = []
    pred_LR = []
    pred_SGD = []
    pred_Bayes = []
    pred_DTree = []
    pred_RForest = []


# DATASET OBJETCS
X = []
y = []
X_train = []
y_train = []
X_test = []
y_test = []